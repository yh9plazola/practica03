package Controlador;
// Paquetes
import Modelo.Gasolina;
import Modelo.Bomba;
import Vista.dlgBomba;
// Mostrar dialogos
import javax.swing.JFrame;
import javax.swing.JOptionPane;
// Eventos
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Controlador implements ActionListener{
    private Bomba bom;
    private dlgBomba vista;
    private int contador;
    
    public Controlador(Bomba bom, dlgBomba vista){
        this.bom = bom;
        this.vista = vista;
        // Escuchar componentes
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnRegistrar.addActionListener(this);
    }
    
    public void iniciarVista(){
        // Configurar la vista
        this.vista.setTitle("BOMBA");
        this.vista.setSize(666, 369);
        this.vista.setVisible(true);
    }
    
    public static void main(String[] args) {
        Bomba obj = new Bomba();
        dlgBomba vista = new dlgBomba(new JFrame(), true);
        Controlador controlador = new Controlador(obj, vista);
        controlador.iniciarVista();
    }
    
    private Boolean isVacio(){
        return this.vista.txtNumBomba.getText().equals("") ||
               this.vista.txtPrecio.getText().equals("");
    }
    
    private int isValidoIniciarBomba(){
        try{
            if(Float.parseFloat(this.vista.txtNumBomba.getText()) <= 0 ||
               Float.parseFloat(this.vista.txtPrecio.getText()) <= 0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
            return 2;
        }
        return 0;
    }
    
    private int isValidoRegistrar(){
        try{
            if(Float.parseFloat(this.vista.txtCantidad.getText()) <= 0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
            return 2;
        }
        return 0;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.vista.btnIniciarBomba){
            if(this.isVacio()){
                JOptionPane.showMessageDialog(this.vista, 
                        "Oye we, tienes campos vacios", 
                        "BOMBA", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isValidoIniciarBomba() == 1){
                JOptionPane.showMessageDialog(this.vista, 
                        "Revisa que no tengas numeros negativos", 
                        "BOMBA", JOptionPane.WARNING_MESSAGE);
                return;
            }
            else if(this.isValidoIniciarBomba() == 2){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error de conversion", "BOMBA", JOptionPane.ERROR_MESSAGE);
                return;
            }            
            this.bom.iniciarBomba(Integer.parseInt(this.vista.txtNumBomba.getText()), 
                    new Gasolina(this.vista.cmbTipo.getSelectedIndex() + 1,
                                 this.vista.cmbTipo.getSelectedItem().toString(),
                                 Float.parseFloat(this.vista.txtPrecio.getText())), this.vista.jSCantidad.getValue(), 0.0f);
            this.vista.btnIniciarBomba.setEnabled(false);
            this.vista.txtNumBomba.setEnabled(false);
            this.vista.txtPrecio.setEnabled(false);
            this.vista.jSCantidad.setEnabled(false);
            this.vista.cmbTipo.setEnabled(false);
            this.vista.txtCantidad.setEnabled(true);
            this.vista.btnRegistrar.setEnabled(true);
        }
        if(e.getSource() == this.vista.btnRegistrar){
            if(this.vista.txtCantidad.getText().equals("")){
                JOptionPane.showMessageDialog(this.vista, 
                        "Oye we, tienes campos vacios", 
                        "BOMBA", JOptionPane.WARNING_MESSAGE);
                return;
            }
            if(this.isValidoRegistrar() == 1){
                JOptionPane.showMessageDialog(this.vista, 
                        "Revisa que no tengas numeros negativos", 
                        "BOMBA", JOptionPane.WARNING_MESSAGE);
                return;
            }
            else if(this.isValidoRegistrar() == 2){
                JOptionPane.showMessageDialog(this.vista, "Ocurrio un error de conversion", "BOMBA", JOptionPane.ERROR_MESSAGE);
                return;
            }
            float venta = this.bom.venderGasolina(Float.parseFloat(this.vista.txtCantidad.getText()));
            if(venta != 0.0f){
                this.vista.lblCosto.setText(String.valueOf(venta));
                this.vista.lblTotalVentas.setText(String.valueOf(this.bom.ventasTotales()));
                this.contador++;
                this.vista.txtContador.setText(String.valueOf(this.contador));
                this.vista.jSCantidad.setValue((int)this.bom.inventarioGasolina());
            }
        }
    }
}