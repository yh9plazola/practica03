package Modelo;

public class Gasolina 
{
    private int IDGasolina;
    private String tipo;
    private float precio;
    
    public Gasolina(){
        this.IDGasolina = 0;
        this.tipo = "";
        this.precio = 0.0f;
    }

    public Gasolina(int IDGasolina, String tipo, float precio) {
        this.IDGasolina = IDGasolina;
        this.tipo = tipo;
        this.precio = precio;
    }
    
    public Gasolina(Gasolina obj){
        this.IDGasolina = obj.IDGasolina;
        this.tipo = obj.tipo;
        this.precio = obj.precio;
    }

    public int getIDGasolina() {
        return IDGasolina;
    }

    public void setIDGasolina(int IDGasolina) {
        this.IDGasolina = IDGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
}