package Modelo;

public class Bomba {
    private int numBomba;
    private Gasolina gas;
    private float capacidad;
    private float acumulador;
    
    public Bomba(){
        this.numBomba = 0;
        this.gas = null;
        this.capacidad = 0.0f;
        this.acumulador = 0.0f;
    }

    public Bomba(int numBomba, Gasolina gas, float capacidad, float acumulador) {
        this.numBomba = numBomba;
        this.gas = gas;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }
    
    public Bomba(Bomba obj){
        this.numBomba = obj.numBomba;
        this.gas = obj.gas;
        this.capacidad = obj.capacidad;
        this.acumulador = obj.acumulador;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public Gasolina getGas() {
        return gas;
    }

    public void setGas(Gasolina gas) {
        this.gas = gas;
    }

    public float getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(float capacidad) {
        this.capacidad = capacidad;
    }

    public float getAcumulador() {
        return acumulador;
    }

    public void setAcumulador(float acumulador) {
        this.acumulador = acumulador;
    }
    
    public void iniciarBomba(int numBomba, Gasolina gas, float capacidad, float acumulador){
        this.numBomba = numBomba;
        this.gas = gas;
        this.capacidad = capacidad;
        this.acumulador = acumulador;
    }
    
    public float inventarioGasolina(){
        return this.capacidad - this.acumulador;
    }
    
    public float venderGasolina(float cantidad){
        if(this.inventarioGasolina() < cantidad){
            return 0.0f;
        }
        this.acumulador += cantidad;
        return cantidad * this.gas.getPrecio();
    }
    
    public float ventasTotales(){
        return this.acumulador * this.gas.getPrecio();
    }
}